//
//  PopularViewPresenter.swift
//  PetProject
//
//  Created by Anton Hoang on 2/20/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation

protocol PopularViewInputProtocol {}
protocol PopularViewOutputProtocol {
    init(view: PopularViewInputProtocol, router: Routable)
}

class PopularViewPresenter: PopularViewOutputProtocol {
    var view: PopularViewInputProtocol
    var router: Routable
    
    required init(view: PopularViewInputProtocol, router: Routable) {
        self.view = view
        self.router = router
    }
}
