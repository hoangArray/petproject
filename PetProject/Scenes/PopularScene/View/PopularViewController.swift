//
//  PopularViewController.swift
//  PetProject
//
//  Created by Anton Hoang on 2/20/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit
import Foundation

private enum Titles {
    case cards
    case collections
}

class PopularViewController: SearchContainerViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: - Properties
    var presenter: PopularViewPresenter?
    var navigationCurtainConstraint: CGFloat!
    
    //MARK: - Lifecycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupSegmentControl()
        selectedSegmentAtIndex(index: 0, title: "COLLECTIONS")
        setupCurtainTitle()
    }
    
    //MARK: - Setup views
    private func setupUI() {
        titleLabel.font = UIFont(name: "Helvetica", size: 15)
        titleLabel.textColor = UIColor(red: 111/255, green: 75/255, blue: 51/255, alpha: 100)
        view.backgroundColor = UIColor(red: 247/255, green: 238/255, blue: 225/255, alpha: 12)
        addContraintToSegmentControl()
        addConstrainToTitleLabel()
        addConstraintToViewContainer()
    }
    
    override func setupCurtainTitle() {
        curtainButton.setTitle("Popular", for: .normal)
    }
    
    private lazy var controllers: [UIViewController] = [self.collectionVC, self.titlesVC]
        
    private let collectionVC: CollectionsViewController = {
        let collectionViewController = CollectionsViewController()
        return collectionViewController
    }()
    
    private let titlesVC: TitlesViewController = {
        let titlesViewController = TitlesViewController()
        return titlesViewController
    }()
    
    private func selectedSegmentAtIndex(index: Int, title: String?) {
        titleLabel.text = title
        children.forEach { removeChildViewController($0) }
        addViewControllerAsChildViewController(self.controllers[index])
    }
    
    private func setupSegmentControl() {
        segmentControl.layer.cornerRadius = 0
        segmentControl.setTitle("Collections", forSegmentAt: 0)
        segmentControl.setTitle("Titles", forSegmentAt: 1)
         
        let selectedColor = UIColor(red: 111/255, green: 75/255, blue: 51/255, alpha: 100)
        let normalTextAttributes: [NSAttributedString.Key : AnyObject] = [
            NSAttributedString.Key.font : UIFont(name: "Helvetica",
                                                 size: 17)!,
            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
        
        let selectedTextAttributes: [NSAttributedString.Key : AnyObject] = [
            NSAttributedString.Key.font : UIFont(name: "Helvetica",
                                                 size: 17)!,
            NSAttributedString.Key.foregroundColor : selectedColor
        ]
        segmentControl.setTitleTextAttributes(normalTextAttributes, for: .normal)
        segmentControl.setTitleTextAttributes(selectedTextAttributes, for: .selected)
        segmentControl.backgroundColor = selectedColor
        segmentControl.addTarget(self, action: #selector(switchAction(_:)), for: .valueChanged)
    }
    
    //MARK: - Setups constraints
    private func addContraintToSegmentControl() {
        segmentControl.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            segmentControl.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            segmentControl.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            segmentControl.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            segmentControl.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    private func addConstrainToTitleLabel() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let titleBeforeLift = titleLabel.topAnchor.constraint(equalTo:
            segmentControl.bottomAnchor, constant: 20)
        titleBeforeLift.isActive = true
        
        let titleAfterLift = titleLabel.topAnchor.constraint(equalTo: navigationCurtain.bottomAnchor)
        titleAfterLift.isActive = false
        
        selectCurtain = { [unowned self] in
            switch self.currentCurtainMode {
            case .lift:
                titleAfterLift.isActive = true
            case .drop:
                titleAfterLift.isActive = false
            }
        }
        
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: segmentControl.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: segmentControl.trailingAnchor),
            titleLabel.heightAnchor.constraint(equalToConstant: 20)
        ])
    }
    
    private func addConstraintToViewContainer() {
        viewContainer.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            viewContainer.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5),
            viewContainer.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            viewContainer.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            viewContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    //MARK: - Actions
    @objc func switchAction(_ sender: UISegmentedControl) {
        selectedSegmentAtIndex(index: sender.selectedSegmentIndex, title: nil)
        let titleLabelsList: [Titles] = [.collections, .cards]
        switch titleLabelsList[sender.selectedSegmentIndex] {
        case .collections:
            return titleLabel.text = "COLLECTIONS"
        case .cards:
            return titleLabel.text = "CARDS"
        }
    }
}

extension PopularViewController {
    private func addViewControllerAsChildViewController(_ childViewController: UIViewController) {
        addChild(childViewController)
        view.addSubview(childViewController.view)
        childViewController.view.frame = viewContainer.bounds
        childViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        childViewController.willMove(toParent: self)
        
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            childViewController.view.topAnchor.constraint(equalTo: viewContainer.topAnchor),
            childViewController.view.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor),
            childViewController.view.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor),
            childViewController.view.bottomAnchor.constraint(equalTo: viewContainer.bottomAnchor)
        ])
    }
    
    private func removeChildViewController(_ childViewController: UIViewController) {
        guard !self.children.isEmpty,
            self.children.contains(childViewController) else { return }
        childViewController.willMove(toParent: nil)
        childViewController.view.removeFromSuperview()
        childViewController.removeFromParent()
    }
}

extension PopularViewController: PopularViewInputProtocol {}

