//
//  TitlesViewController.swift
//  PetProject
//
//  Created by Anton Hoang on 2/20/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit

class TitlesViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
        
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - Setup views
    private func setupUI() {
        view.backgroundColor = UIColor(red: 247/255, green: 238/255, blue: 225/255, alpha: 12)
        tableView.layer.cornerRadius = 10
        tableView.showsVerticalScrollIndicator = false 
        tableView.backgroundColor = .clear
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    func petList() -> [Pet] {
        let petsList = [Pet(petImage: "Image", petNameLabel: "Airedale Terrier", petDetailsLabel: "The Airedale is the largest of the British terriers."),
                        Pet(petImage: "Image-1", petNameLabel: "Aidi", petDetailsLabel: "Dogs of the Affenpinscher type have been known …"),
                        Pet(petImage: "Image-2", petNameLabel: "Afghan Hound", petDetailsLabel: "The Airedale Terrier is a of the now."),
                        Pet(petImage: "Image-3", petNameLabel: "Aidi", petDetailsLabel: "The Airedale Terrier is a descendant of the now…"),
                        Pet(petImage: "Image-4", petNameLabel: "Akbash", petDetailsLabel: "Akbash is accumalate him know how to accurate godzilla"),
                        Pet(petImage: "Image-5", petNameLabel: "Affenpinscher", petDetailsLabel: "Dogs of the Affenpinscher type have been known …")]
          return petsList
      }
}

extension TitlesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return petList().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        let items = petList()[indexPath.row].petNameLabel
        cell.textLabel?.text = items
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
}
