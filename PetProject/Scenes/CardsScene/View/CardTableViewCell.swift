//
//  CardTableViewCell.swift
//  PetProject
//
//  Created by Anton Hoang on 2/27/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit

class CardTableViewCell: UICollectionViewCell, PetCellConfiguratable {
    
    //MARK: - IBOutlets
    @IBOutlet weak var petImage: UIImageView!
    @IBOutlet weak var petName: UILabel!
    @IBOutlet weak var petDetail: UILabel!
    @IBOutlet weak var petView: UIView!
    @IBOutlet weak var cardBackgroundView: UIView!
    
    //MARK: - Lifecicle
    override func awakeFromNib() {
        super.awakeFromNib()
        petView.layer.cornerRadius = 10
        cardBackgroundView.layer.shadowColor = UIColor.black.cgColor
        cardBackgroundView.layer.shadowOpacity = 0.5
        cardBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 1)
        cardBackgroundView.layer.shadowRadius = 1
        backgroundColor = UIColor(red: 247/255, green: 238/255, blue: 225/255, alpha: 12)
        cardBackgroundView.layer.cornerRadius = 10
    }
    
    //MARK: - set a cells
    func configurate(pet: Pet) {
        petImage.contentMode = .scaleAspectFill
        petImage.image = UIImage(named: pet.petImage)
        petName.text = pet.petNameLabel
        petDetail.text = pet.petDetailsLabel
    }
}

