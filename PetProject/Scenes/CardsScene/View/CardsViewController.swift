//
//  CardsViewController.swift
//  PetProject
//
//  Created by Anton Hoang on 2/13/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit
import Foundation
import CoreGraphics

enum LayoutMode {
    case card
    case list
    
    mutating func tooggle() {
        switch self {
        case .card: self = .list
        case .list: self = .card
        }
    }
}

class CardsViewController: SearchContainerViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - Properties
    var presenter: CardsViewPresenter!
    var selectedCell: CardCollectionViewCell!
    private var initialFrame: CGRect?
    private var selectedCellIndex: Int!
    private var currentScrollingIndex: Int = 1
    private var cellCoordinateByX: CGFloat!
    private var diff_points: CGFloat!
    private var currentLayoutMode: LayoutMode = .card
    
    private lazy var animator: UIViewPropertyAnimator = {
        return UIViewPropertyAnimator(duration: 0.5, curve: .easeInOut)
    }()
    
    //MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        setupUI()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - setup UI
    private func setupUI() {
        counterLabel.textColor = UIColor(red: 111/255, green: 75/255, blue: 51/255, alpha: 100)
        counterLabel.text = "\(currentScrollingIndex)/\(presenter.petList.count)"
        view.backgroundColor = UIColor(red: 247/255, green: 238/255, blue: 225/255, alpha: 12)
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.leftAnchor.constraint(equalTo: view.leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: view.rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: counterLabel.topAnchor)
        ])
    }
    private func setupCollectionView() {
        collectionView.register(CardCollectionViewCell.self,
                                forCellWithReuseIdentifier: "CardCollectionViewCell")
        collectionView.register(UINib(nibName: "CardTableViewCell",
                                      bundle: nil),
                                forCellWithReuseIdentifier: "CardTableViewCell")
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.collectionViewLayout = PetCollectionViewFlowLayout()
        self.blockButton.image = UIImage(named: "blockCard")

        selectBlock = { [unowned self] in
            self.currentLayoutMode.tooggle()
            let layout = self.getCollectionLayout()
            
            UIView.animate(withDuration: 1.2, delay: 0.5, usingSpringWithDamping: 20, initialSpringVelocity: 10, options: .curveEaseInOut, animations: {
                self.collectionView.setCollectionViewLayout(layout, animated: true)
                self.collectionView.reloadItems(at: self.collectionView.indexPathsForVisibleItems)
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    private func getCollectionLayout() -> UICollectionViewLayout {
        switch currentLayoutMode {
        case .card:
            self.blockButton.image = UIImage(named: "blockCard")
            return PetCollectionViewFlowLayout()
        case .list:
            self.blockButton.image = UIImage(named: "block")
            return PetTableViewFlowLayout()
        }
    }
}


extension CardsViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    //MARK: - CollectionView setups
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.petList.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let pet = presenter.petList[indexPath.row]
        var cell: (UICollectionViewCell & PetCellConfiguratable)!
        switch currentLayoutMode {
        case .card:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCollectionViewCell",
                                                      for: indexPath) as! CardCollectionViewCell
        case .list:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardTableViewCell",
                                                      for: indexPath) as! CardTableViewCell
        }
        cell.configurate(pet: pet)
        return cell
    }
    
    //MARK: - Collections view actions
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        selectedCell = collectionView.cellForItem(at: indexPath) as? CardCollectionViewCell
        let currentCard = presenter.petList[indexPath.row]
        
        //config current detail
        selectedCellIndex = indexPath.row
        expand(card: currentCard)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = collectionView.indexPathForItem(at: visiblePoint) else { return }
        currentScrollingIndex = indexPath.item
        counterLabel.text = "\(indexPath.item + 1)/\(presenter.petList.count)"
    }
}

extension CardsViewController: CardsViewInputProtocol {}

extension CardsViewController {
    //MARK: - Expand collection view each time did select
    func expand(card: Pet) {
        self.navigationController?.isNavigationBarHidden = true
        animator.addAnimations {
            self.moveNeghboringCells(-, +)
        }
        animator.addCompletion { [unowned self] (position) in
            switch position {
            case .end:
                self.collectionView.isScrollEnabled = false
                self.presenter?.showCardDetail(currentCard: card)
            default:
                break
            }
        }
        animator.startAnimation()
    }
    
    //MARK: - Collapse animation when Pop to this view controller
    func collapse() {
        self.navigationController?.isNavigationBarHidden = false
        animator.addAnimations { [unowned self] in
            //diff - number of pt(points) to move
            self.moveNeghboringCells(+, -)
        }
        animator.addCompletion { (position) in
            switch position {
            case .end:
                self.collectionView.isScrollEnabled = true
            default:
                break
            }
        }
        animator.startAnimation()
    }
    
    //MARK: - move neghboring cells on appropriate side
    private func moveNeghboringCells(_ signLeftCell: (CGFloat, CGFloat) -> (CGFloat),
                                     _ signRightCell: (CGFloat, CGFloat) -> (CGFloat)) {
        if let leftCell = collectionView.cellForItem(at: IndexPath(row: self.selectedCellIndex - 1,
                                                                   section: 0)) {
            let result =  signLeftCell(leftCell.center.x, 50)
            leftCell.center.x = result
        }
        
        if let rightCell = collectionView.cellForItem(at: IndexPath(row: self.selectedCellIndex + 1,
                                                                    section: 0)) {
            let result = signRightCell(rightCell.center.x, 50)
            rightCell.center.x = result
        }
    }
    
    //MARK: - override math sign to pass signs as parameter -> moveNeghboringCells(+, -)
    static private func +(lhs: CardsViewController, rhs: CardsViewController) -> CGFloat {
        let res = (lhs.cellCoordinateByX) + (rhs.diff_points)
        return res
    }
    
    static private func -(lhs: CardsViewController, rhs: CardsViewController) -> CGFloat {
        let res = (lhs.cellCoordinateByX) - (rhs.diff_points)
        return res
    }
}
