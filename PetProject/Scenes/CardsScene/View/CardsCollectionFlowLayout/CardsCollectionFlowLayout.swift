//
//  CardsCollectionFlowLayout.swift
//  PetProject
//
//  Created by Anton Hoang on 2/18/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation
import UIKit

class PetTableViewFlowLayout: UICollectionViewFlowLayout {

    override func prepare() {
        super.prepare()
        guard let collectionSize = collectionView?.frame.size else { return }
        scrollDirection = .vertical
        minimumLineSpacing = 20
        itemSize = CGSize(width: collectionSize.width, height: collectionSize.height / 4.5)
    }
}

class PetCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    let activeDistance: CGFloat = 200
    let zoomFactor: CGFloat = 0.3
    
    override func prepare() {
        super.prepare()
        guard let size = collectionView?.frame.size else { return }
        scrollDirection = .horizontal
        minimumLineSpacing = 50
        sectionInset = UIEdgeInsets(top: 0, left: 75, bottom: 0, right: 75)
        itemSize = CGSize(width: size.width * 0.63, height: size.height * 0.65)
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let collectionView = collectionView else { return nil }
        let rectAttributes = super.layoutAttributesForElements(in: rect)!
            .map {
            $0.copy() as! UICollectionViewLayoutAttributes
        }
        let visibleRect = CGRect(origin: collectionView.contentOffset,
                                 size: collectionView.frame.size)
        
        // Make the cells be zoomed when they reach the center of the screen
        for attributes in rectAttributes where attributes.frame.intersects(visibleRect) {
            let distance = visibleRect.midX - attributes.center.x
            let normalizedDistance = distance / activeDistance
            
            if distance.magnitude < activeDistance {
                let zoom = 1 + zoomFactor * (1 - normalizedDistance.magnitude)
                attributes.transform3D = CATransform3DMakeScale(zoom, zoom, 1)
                attributes.zIndex = Int(zoom.rounded())
            }
        }
        return rectAttributes
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint,
                                      withScrollingVelocity velocity: CGPoint) -> CGPoint {
        guard let collectionView = collectionView else { return .zero }
        
        // Add some snapping behaviour so that the zoomed cell is always centered
        let targetRect = CGRect(x: proposedContentOffset.x,
                                y: 0,
                                width: collectionView.frame.width,
                                height: collectionView.frame.height)
        
        guard let rectAttributes = super.layoutAttributesForElements(in: targetRect)
            else { return .zero }
        
        var offsetAdjustment = CGFloat.greatestFiniteMagnitude
        let horizontalCenter = proposedContentOffset.x + collectionView.frame.width / 2
        
        for layoutAttributes in rectAttributes {
            let itemHorizontalCenter = layoutAttributes.center.x
            if (itemHorizontalCenter - horizontalCenter).magnitude < offsetAdjustment.magnitude {
                offsetAdjustment = itemHorizontalCenter - horizontalCenter
            }
        }
        
        return CGPoint(x: proposedContentOffset.x + offsetAdjustment,
                       y: proposedContentOffset.y)
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        // Invalidate layout so that every cell get a chance to be zoomed when it reaches the center of the screen
        return true
    }
}
