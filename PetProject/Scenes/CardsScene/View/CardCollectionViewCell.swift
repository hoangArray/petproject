//
//  CardViewCell.swift
//  PetProject
//
//  Created by Anton Hoang on 2/14/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell, PetCellConfiguratable {
    
    //MARK: - Properties
    var backCardView: UIView!
    var expandStackview: UIStackView!
    var petImage: UIImageView!
    var petDescription: UIView!
    var petNameLabel: UILabel!
    var petDetailsLabel: UILabel!
    
    //MARK: - lifecicle
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        createStackViewContent()
        createExpandStackView()
        createBackCardView()
        createPetDescriptionContent()
        adaptStackViewToCollectionCardViewStyle()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func configurate(pet: Pet) {
        self.petImage.contentMode = .scaleAspectFit
        self.petImage.image = UIImage(named: pet.petImage)
        self.petNameLabel.text = pet.petNameLabel 
        self.petDetailsLabel.text = pet.petDetailsLabel
    }
    
    //MARK: - createExpandStackView
    private func createExpandStackView() {
        expandStackview = UIStackView()
        expandStackview.spacing = 0
        expandStackview.addArrangedSubview(petImage)
        expandStackview.addArrangedSubview(petDescription)
        expandStackview.translatesAutoresizingMaskIntoConstraints = false
    }
    
    //MARK: - CreatePetDescriptionContent
     private func createPetDescriptionContent() {
         petNameLabel = UILabel()
         petNameLabel.text = "Djek russell terrier"
         petNameLabel.numberOfLines = 0
         petNameLabel.font = UIFont(name: "Futura", size: 17)
         petNameLabel.textColor = .brown
         petNameLabel.translatesAutoresizingMaskIntoConstraints = false
         
         petDetailsLabel = UILabel()
         petDetailsLabel.text = "Jack Russells are an energetic breed that rely on a high level of exercise …"
         petDetailsLabel.numberOfLines = 0
         petDetailsLabel.font = UIFont(name: "Helvetica", size: 12)
         petDetailsLabel.textColor = .brown
         petDetailsLabel.translatesAutoresizingMaskIntoConstraints = false
         
         petDescription.addSubview(petNameLabel)
         petDescription.addSubview(petDetailsLabel)
     }
     
     //MARK: - CreateStackViewContent
     private func createStackViewContent() {
         let image = UIImage(named: "Group Copy 3")
         petImage = UIImageView(image: image)
         petDescription = UIView()
         petDescription.backgroundColor = .white
         petDescription.layer.cornerRadius = 10
         petDescription.layer.maskedCorners = [.layerMaxXMaxYCorner,
                                               .layerMaxXMinYCorner,
                                               .layerMinXMaxYCorner,
                                               .layerMinXMinYCorner]
     }
     
     //MARK: - SetupBackCardView
     private func createBackCardView() {
         backCardView = UIView()
         backCardView.backgroundColor = .white
         backCardView.layer.cornerRadius = 10
         backCardView.layer.maskedCorners = [.layerMaxXMaxYCorner,
                                             .layerMaxXMinYCorner,
                                             .layerMinXMaxYCorner,
                                             .layerMinXMinYCorner]
         backCardView.translatesAutoresizingMaskIntoConstraints = false
         backCardView.addSubview(expandStackview)
         addSubview(backCardView)
     }
    
    private func commonInit() {
        backgroundColor = .clear
    }
    
    //MARK: - adaptStackView to CollectionView card style
     func adaptStackViewToCollectionCardViewStyle() {
         settingStackViewForAdaptStackView(axis: .vertical,
                                           alignment: .center,
                                           distribution: .fill)
         settingCollectionViewStyleBackCardView()
         settingCollectionViewStylePetImage()
         settingCollectionViewStyleStackView()
         settingCollectionViewStylePetDescription()
         settingCollectionViewStylePetDescriptionContentView()
     }
    
    //MARK: - ALL SETTINGS FOR ADAPT STACKVIEW TO COLLECTIONVIEW CARD STYLE ========================
    //MARK: - addConstraintToPetDescription
    private func settingCollectionViewStylePetDescription() {
        petDescription.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            petDescription.topAnchor.constraint(equalTo: petImage.bottomAnchor),
            petDescription.leadingAnchor.constraint(equalTo: expandStackview.leadingAnchor),
            petDescription.trailingAnchor.constraint(equalTo: expandStackview.trailingAnchor),
            petDescription.heightAnchor.constraint(equalToConstant: petImage.frame.size.height / 4),
            petDescription.bottomAnchor.constraint(equalTo: expandStackview.bottomAnchor)
        ])
    }
    //MARK: - addConstraintToPetImage
    private func settingCollectionViewStylePetImage() {
        petImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            petImage.topAnchor.constraint(equalTo: expandStackview.topAnchor),
            petImage.leadingAnchor.constraint(equalTo: expandStackview.leadingAnchor, constant: 10),
            petImage.trailingAnchor.constraint(equalTo: expandStackview.trailingAnchor, constant: -10)
        ])
    }
    //MARK: - addConstraintToStackView
    private func settingCollectionViewStyleStackView() {
        NSLayoutConstraint.activate([
            expandStackview.topAnchor.constraint(equalTo: topAnchor),
            expandStackview.leadingAnchor.constraint(equalTo: leadingAnchor),
            expandStackview.trailingAnchor.constraint(equalTo: trailingAnchor),
            expandStackview.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    //MARK: - addConstraintToPetDescriptionContentView
    private func settingCollectionViewStylePetDescriptionContentView() {
        petNameLabel.textAlignment = .center
        NSLayoutConstraint.activate([
            petNameLabel.topAnchor.constraint(equalTo: petDescription.topAnchor),
            petNameLabel.leadingAnchor.constraint(equalTo: petDescription.leadingAnchor),
            petNameLabel.trailingAnchor.constraint(equalTo: petDescription.trailingAnchor)
        ])
        
        petDetailsLabel.textAlignment = .center
        NSLayoutConstraint.activate([
            petDetailsLabel.topAnchor.constraint(equalTo: petNameLabel.bottomAnchor),
            petDetailsLabel.leadingAnchor.constraint(equalTo: petNameLabel.leadingAnchor),
            petDetailsLabel.trailingAnchor.constraint(equalTo: petNameLabel.trailingAnchor)
        ])
    }
    
    //MARK: - AddConstraintToBackCardView
    private func settingCollectionViewStyleBackCardView() {
        NSLayoutConstraint.activate([
            backCardView.topAnchor.constraint(equalTo: expandStackview.topAnchor,
                                              constant: petImage.frame.size.height / 1.7),
            backCardView.bottomAnchor.constraint(equalTo: expandStackview.bottomAnchor),
            backCardView.leadingAnchor.constraint(equalTo: expandStackview.leadingAnchor),
            backCardView.trailingAnchor.constraint(equalTo: expandStackview.trailingAnchor)
        ])
    }
    
    //MARK: - Settings for CollectionView and Tableview styles
    private func settingStackViewForAdaptStackView(axis: NSLayoutConstraint.Axis,
                                                   alignment: UIStackView.Alignment,
                                                   distribution: UIStackView.Distribution) {
        expandStackview.axis = axis
        expandStackview.alignment = alignment
        expandStackview.distribution = distribution
        expandStackview.backgroundColor = .clear
    }
}

protocol PetCellConfiguratable: class {
    func configurate(pet: Pet)
}
