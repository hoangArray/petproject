//
//  CardsViewPresenter.swift
//  PetProject
//
//  Created by Anton Hoang on 2/18/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation
import UIKit

protocol CardsViewInputProtocol {}
protocol CardsViewOutputProtocol {
    
    init(view: CardsViewInputProtocol, router: Routable)
    func showCardDetail(currentCard: Pet)
}

class CardsViewPresenter: CardsViewOutputProtocol {
    
    var view: CardsViewInputProtocol
    var router: Routable
    
    required init(view: CardsViewInputProtocol, router: Routable) {
        self.view = view
        self.router = router
    }

    func showCardDetail(currentCard: Pet) {
        router.showDetailScene(currentCard)
    }
    
    let petList = [Pet(petImage: "Group Copy 3", petNameLabel: "Airedale Terrier", petDetailsLabel: "The Airedale is the largest of the British terriers."),
                   Pet(petImage: "Group copy4", petNameLabel: "Aidi", petDetailsLabel: "Dogs of the Affenpinscher type have been known …"),
                   Pet(petImage: "Image-2", petNameLabel: "Afghan Hound", petDetailsLabel: "The Airedale Terrier is a of the now."),
                   Pet(petImage: "Image-3", petNameLabel: "Aidi", petDetailsLabel: "The Airedale Terrier is a descendant of the now…"),
                   Pet(petImage: "Image-4", petNameLabel: "Akbash", petDetailsLabel: "Akbash is accumalate him know how to accurate godzilla"),
                   Pet(petImage: "Image-6", petNameLabel: "Affenpinscher", petDetailsLabel: "Dogs of the Affenpinscher type have been known …"),
                   Pet(petImage: "Image-7", petNameLabel: "Affenpinscher", petDetailsLabel: "Dogs of the Affenpinscher type have been known …")]
}
