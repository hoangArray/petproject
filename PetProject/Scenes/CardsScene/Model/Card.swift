//
//  Cards.swift
//  PetProject
//
//  Created by Anton Hoang on 2/25/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation

struct Card {
    var petImage: String
    var petDetailsLabel: String
    var petNameLabel: String
}
