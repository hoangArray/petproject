//
//  CollectionsViewController.swift
//  PetProject
//
//  Created by Anton Hoang on 2/20/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit

class CollectionsViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Lifecicle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - Setup views
    private func setupUI() {
        view.backgroundColor = UIColor(red: 247/255, green: 238/255, blue: 225/255, alpha: 12)
        tableView.showsVerticalScrollIndicator = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "FavoritesViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.backgroundColor = .clear        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    func petList() -> [Pet] {
        let petsList = [Pet(petImage: "Image", petNameLabel: "Airedale Terrier", petDetailsLabel: "The Airedale is the largest of the British terriers."),
                        Pet(petImage: "Image-1", petNameLabel: "Aidi", petDetailsLabel: "Dogs of the Affenpinscher type have been known …"),
                        Pet(petImage: "Image-2", petNameLabel: "Afghan Hound", petDetailsLabel: "The Airedale Terrier is a of the now."),
                        Pet(petImage: "Image-3", petNameLabel: "Aidi", petDetailsLabel: "The Airedale Terrier is a descendant of the now…"),
                        Pet(petImage: "Image-4", petNameLabel: "Akbash", petDetailsLabel: "Akbash is accumalate him know how to accurate godzilla"),
                        Pet(petImage: "Image-5", petNameLabel: "Affenpinscher", petDetailsLabel: "Dogs of the Affenpinscher type have been known …")]
        return petsList
    }
}

extension CollectionsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return petList().count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FavoritesViewCell
        let items = petList()[indexPath.section]
        cell.configurate(pets: items)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = .clear
        return footerView
    }
}
