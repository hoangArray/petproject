//
//  DetailViewController.swift
//  PetProject
//
//  Created by Anton Hoang on 2/14/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit

enum Favourite {
    case like
    case dislike
    
    mutating func toggle() {
        switch self {
        case .like: self = .dislike
        case .dislike: self = .like
        }
    }
}
extension DetailViewController: DetailViewInputProtocol {
    func config() {
        self.petCardImage_1.image = UIImage(named: presenter?.card?.petImage ?? "")
        self.petNameLabel.text = presenter?.card?.petNameLabel
    }
}


class DetailViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var petCardImage_1: UIImageView!
    @IBOutlet weak var petNameLabel: UILabel!
    @IBOutlet weak var petDescription: UIView!
    @IBOutlet weak var petDescriptionLine: UIView!
    @IBOutlet weak var petDescriptionView: UITextView!
    @IBOutlet weak var petDetailsView_1: UITextView!
    @IBOutlet weak var petCardImage_2: UIImageView!
    @IBOutlet weak var petDetailsView_2: UITextView!
    @IBOutlet weak var petCardImage_3: UIImageView!
    @IBOutlet weak var petDetailsView_3: UITextView!
    @IBOutlet weak var petCardImage_4: UIImageView!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var addToFavorites: UIButton!
    private var favourite: Favourite = .dislike
    
    //MARK: - Properties
    var presenter: DetailViewPresenter?
    var favoritesList = [Pet]()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupScrollView()
        setupPetCardImage()
        setupPetNameLabel()
        setupPetDescription()
        setupPetDescriptionView()
        setupPetDescriptionLine()
        setupPetDetailsView_1()
        setupPetCardImage_2()
        setupPetDetailsView_2()
        setupPetCardImage_3()
        setupPetDetailsView_3()
        setupPetCardImage_4()
        setupDismissButton()
        setupAddToFavorites()
        presenter?.configurateDetail()
    }
    
    @objc private func dismissAction(_ sender: Any) {
        presenter?.addCardToFavorites(cards: favoritesList)
        presenter?.dismiss()
    }
    
    @objc private func addToFavoritesAction(_ sender: Any) {
        guard let pet = presenter?.card else { return }
        favourite.toggle()
        switch favourite {
        case .like:
            appendPet(image: "favorites_selected", pet: pet)
        case .dislike:
            deletePet(image: "favorites_not_selected", pet: pet)
        }
    }
    
    private func appendPet(image: String, pet: Pet) {
        setupRatePet(image: image)
        favoritesList.append(pet)
    }
    private func deletePet(image: String, pet: Pet) {
        setupRatePet(image: image)
        guard let index = favoritesList.firstIndex(where: { $0.petImage == pet.petImage })
            else { return }
        favoritesList.remove(at: index)
    }
    
    private func setupRatePet(image: String) {
        let selected = UIImage(named: "favorites_selected")
        addToFavorites.setImage(selected, for: .normal)
    }
    
    //MARK: - set scrollView
    private func setupScrollView() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    //MARK: - set addToFavorites
    private func setupAddToFavorites() {
        addToFavorites.addTarget(self, action: #selector(addToFavoritesAction(_:)), for: .touchUpInside)
        addToFavorites.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            addToFavorites.topAnchor.constraint(equalTo: dismissButton.topAnchor),
            addToFavorites.trailingAnchor.constraint(equalTo: petCardImage_1.trailingAnchor, constant: -10),
            addToFavorites.heightAnchor.constraint(equalToConstant: 50),
            addToFavorites.heightAnchor.constraint(equalTo: addToFavorites.widthAnchor)
        ])
    }
    
    //MARK: - set dismissButton
    private func setupDismissButton() {
        dismissButton.addTarget(self, action: #selector(dismissAction(_:)), for: .touchUpInside)
        dismissButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            dismissButton.topAnchor.constraint(equalTo: petCardImage_1.topAnchor, constant: 10),
            dismissButton.leadingAnchor.constraint(equalTo: petCardImage_1.leadingAnchor, constant: 10),
            dismissButton.heightAnchor.constraint(equalToConstant: 50),
            dismissButton.heightAnchor.constraint(equalTo: dismissButton.widthAnchor)
        ])
    }
    
    //MARK: - set petCardImage
    private func setupPetCardImage() {
        petCardImage_1.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            petCardImage_1.topAnchor.constraint(equalTo: scrollView.topAnchor),
            petCardImage_1.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            petCardImage_1.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            petCardImage_1.heightAnchor.constraint(equalTo: petCardImage_1.widthAnchor, multiplier: 1.2),
            petCardImage_1.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    //MARK: - set petNameLabel
    private func setupPetNameLabel() {
        petNameLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            petNameLabel.topAnchor.constraint(equalTo: petCardImage_1.bottomAnchor),
            petNameLabel.leadingAnchor.constraint(equalTo: petCardImage_1.leadingAnchor),
            petNameLabel.trailingAnchor.constraint(equalTo: petCardImage_1.trailingAnchor),
            petNameLabel.heightAnchor.constraint(equalToConstant: 100),
            petNameLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    //MARK: - set petDescription
    private func setupPetDescription() {
        petDescription.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            petDescription.topAnchor.constraint(equalTo: petNameLabel.bottomAnchor),
            petDescription.leadingAnchor.constraint(equalTo: petNameLabel.leadingAnchor),
            petDescription.trailingAnchor.constraint(equalTo: petNameLabel.trailingAnchor),
            petDescription.heightAnchor.constraint(equalToConstant: 85),
            petDescription.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    //MARK: - set petDescriptionView
    private func setupPetDescriptionView() {
        petDescriptionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            petDescriptionView.topAnchor.constraint(equalTo: petDescription.topAnchor),
            petDescriptionView.leadingAnchor.constraint(equalTo: petDescriptionLine.trailingAnchor, constant: 12),
            petDescriptionView.trailingAnchor.constraint(equalTo: petDescription.trailingAnchor),
            petDescriptionView.bottomAnchor.constraint(equalTo: petDescription.bottomAnchor)
        ])
    }
    //MARK: - set petDescriptionLine
    private func setupPetDescriptionLine() {
        petDescriptionLine.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            petDescriptionLine.topAnchor.constraint(equalTo: petDescription.topAnchor, constant: 10),
            petDescriptionLine.leadingAnchor.constraint(equalTo: petDescription.leadingAnchor, constant: 12),
            petDescriptionLine.widthAnchor.constraint(equalToConstant: 5),
            petDescriptionLine.bottomAnchor.constraint(equalTo: petDescriptionView.bottomAnchor)
        ])
    }
    
    //MARK: - set petDetailsView_1
    private func setupPetDetailsView_1() {
        petDetailsView_1.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            petDetailsView_1.topAnchor.constraint(equalTo: petDescription.bottomAnchor),
            petDetailsView_1.leadingAnchor.constraint(equalTo: petDescription.leadingAnchor),
            petDetailsView_1.trailingAnchor.constraint(equalTo: petDescription.trailingAnchor),
            petDetailsView_1.heightAnchor.constraint(equalToConstant: 200),
            petDetailsView_1.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    //MARK: - set petCardImage_2
    private func setupPetCardImage_2() {
        petCardImage_2.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            petCardImage_2.topAnchor.constraint(equalTo: petDetailsView_1.bottomAnchor),
            petCardImage_2.leadingAnchor.constraint(equalTo: petDetailsView_1.leadingAnchor),
            petCardImage_2.trailingAnchor.constraint(equalTo: petDetailsView_1.trailingAnchor),
            petCardImage_2.heightAnchor.constraint(equalTo: petCardImage_2.widthAnchor, multiplier: 1),
            petCardImage_2.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    //MARK: - set petDetailsView_2
    private func setupPetDetailsView_2() {
        petDetailsView_2.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            petDetailsView_2.topAnchor.constraint(equalTo: petCardImage_2.bottomAnchor),
            petDetailsView_2.leadingAnchor.constraint(equalTo: petCardImage_2.leadingAnchor),
            petDetailsView_2.trailingAnchor.constraint(equalTo: petCardImage_2.trailingAnchor),
            petDetailsView_2.heightAnchor.constraint(equalToConstant: 200),
            petDetailsView_2.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    //MARK: - set petCardImage_3
    private func setupPetCardImage_3() {
        petCardImage_3.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            petCardImage_3.topAnchor.constraint(equalTo: petDetailsView_2.bottomAnchor),
            petCardImage_3.leadingAnchor.constraint(equalTo: petDetailsView_2.leadingAnchor),
            petCardImage_3.trailingAnchor.constraint(equalTo: petDetailsView_2.trailingAnchor),
            petCardImage_3.heightAnchor.constraint(equalTo: petCardImage_3.widthAnchor, multiplier: 1),
            petCardImage_3.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    //MARK: - set petDetailsView_3
    private func setupPetDetailsView_3() {
        petDetailsView_3.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            petDetailsView_3.topAnchor.constraint(equalTo: petCardImage_3.bottomAnchor),
            petDetailsView_3.leadingAnchor.constraint(equalTo: petCardImage_3.leadingAnchor),
            petDetailsView_3.trailingAnchor.constraint(equalTo: petCardImage_3.trailingAnchor),
            petDetailsView_3.heightAnchor.constraint(equalToConstant: 200),
            petDetailsView_3.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    //MARK: - set petCardImage_4
    private func setupPetCardImage_4() {
        petCardImage_4.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            petCardImage_4.topAnchor.constraint(equalTo: petDetailsView_3.bottomAnchor),
            petCardImage_4.leadingAnchor.constraint(equalTo: petDetailsView_3.leadingAnchor),
            petCardImage_4.trailingAnchor.constraint(equalTo: petDetailsView_3.trailingAnchor),
            petCardImage_4.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            petCardImage_4.heightAnchor.constraint(equalTo: petCardImage_4.widthAnchor, multiplier: 1),
            petCardImage_4.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    //MARK: - Setup views
    private func setupUI() {
        self.view.backgroundColor = UIColor(red: 111/255, green: 75/255, blue: 51/255, alpha: 1)
        scrollView.backgroundColor = .white
        scrollView.layer.cornerRadius = 15
        
        let image = UIImage(named: "Group Copy 3")
        let image_2 = UIImage(named: "dogBall")
        let image_3 = UIImage(named: "dogSmall")
        let image_4 = UIImage(named: "dogEyes")
        
        petCardImage_1.image = image
        petCardImage_1.contentMode = .scaleAspectFill
        petCardImage_2.image = image_2
        petCardImage_3.image = image_3
        petCardImage_4.image = image_4
        
        petNameLabel.textColor = UIColor(red: 116/255, green: 74/255, blue: 45/255, alpha: 1)
        petNameLabel.font = UIFont(name: "Futura", size: 30)
    }
}

extension DetailViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.contentOffset.x = 0
    }
}
