//
//  DetailViewPresenter.swift
//  PetProject
//
//  Created by Anton Hoang on 2/19/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation

protocol DetailViewInputProtocol {
    func config()
}

protocol DetailViewOutputProtocol {
    init(view: DetailViewInputProtocol, router: Routable, withCard: Pet)
    func dismiss()
    func addCardToFavorites(cards: [Pet]?)
}

class DetailViewPresenter: DetailViewOutputProtocol {
    
    var view: DetailViewInputProtocol?
    var router: Routable?
    var card: Pet?
    
    required init(view: DetailViewInputProtocol, router: Routable, withCard: Pet) {
        self.view = view
        self.router = router
        self.card = withCard
    }
    
    func configurateDetail() {
        view?.config()
    }
    
    func dismiss() {
        router?.popToCardScene()
    }
    
    func addCardToFavorites(cards: [Pet]?) {
        router?.petList = cards
    }
}
