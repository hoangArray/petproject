//
//  NavigationItemCell.swift
//  PetProject
//
//  Created by Anton Hoang on 2/15/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit

class NavigationItemCell: UICollectionViewCell {

    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var itemImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configurate(item: CategoryItem) {
        let im = UIImage(named: item.image)
        itemImage.image = im
        itemImage.backgroundColor = .clear
        itemLabel.text = item.title
        itemLabel.textColor = .white
        itemLabel.font = UIFont.init(name: "Helvetica", size: 15)
        itemLabel.backgroundColor = .clear
    }
}

struct CategoryItem {
    var image: String
    var title: String
}
