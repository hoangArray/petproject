//
//  BaseViewController.swift
//  PetProject
//
//  Created by Anton Hoang on 2/16/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit
import Foundation

enum CurtainMode {
    case lift
    case drop
    
    mutating func toogle() {
        switch self {
        case .lift: self = .drop
        case .drop: self = .lift
        }
    }
}

class SearchContainerViewController: UIViewController {
    //MARK: - Properties
    var navigationCurtain = NavigationCurtainView()
    var currentCurtainMode: CurtainMode = .drop
    
    private let searchbarView = SearchbarView()
    private let searchTableView = UITableView()
    private var heightCurtainAnchor: NSLayoutConstraint?
    private var rightConstraint: NSLayoutConstraint!
    private var heightNavCurtainExpand: NSLayoutConstraint!
    private var heightNavCurtainCollapse: NSLayoutConstraint!
    
    var selectCurtain: (() -> ())?
    var selectBlock: (() -> ())?
    
    private var filteredPetsList = [String]()
    private var searchBarIsEmpty: Bool {
        guard let text = searchbarView.searchController.searchBar.text else { return false }
        return text.isEmpty
    }
    
    private var isFiltering: Bool {
        return searchbarView.searchController.isActive && !searchBarIsEmpty
    }
    
    var petsList = [ "Airedale Terrier","Aidi","Afghan Hound", "Aidi", "Akbash", "Affenpinscher",
                     "Barbet","Basenji", "Bassador", "Basset Fauve de Bretagne","Basset Hound",
                     "Basset Retriever","Bavarian Mountain Scent Hound","Beabull","Dachsador",
                     "Dachshund", "Dalmatian","Dandie Dinmont Terrier"]

    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarView()
        setupNavigationItems()
        addConstraintToView()
        setupExpandableSearchbar()
        setupSearchControllerView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cancelSearch()
    }
    
    //MARK: - Setup search table view
    private func setupSearchControllerView() {
        searchbarView.searchController.searchBar.delegate = self
        searchbarView.searchController.searchResultsUpdater = self
    }
    
    private func setupSearchTableView() {
        searchTableView.dataSource = self
        searchTableView.delegate = self
        searchTableView.layer.cornerRadius = 10
        searchTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        searchTableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(searchTableView)
        
        NSLayoutConstraint.activate([
            searchTableView.topAnchor.constraint(equalTo: navigationCurtain.bottomAnchor, constant: 10),
            searchTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            searchTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            searchTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    //MARK: - Setup cancel button when searchbar is open
    lazy var cancelSearchButton: UIBarButtonItem = {
        let cancel = UIButton(type: .custom)
        cancel.addTarget(self, action: #selector(cancelSearch), for: .touchUpInside)
        cancel.setTitle("Cancel", for: .normal)
        cancel.sizeToFit()
        let cb = UIBarButtonItem(customView: cancel)
        cb.tintColor = .white
        return cb
    }()
    
    lazy var blockButton: UIBarButtonItem = {
        let blockBut = UIBarButtonItem(image: UIImage(named:"block"), style: .done, target: self, action: #selector(blockAction))
        blockBut.tintColor = .white
        return blockBut
    }()
    
    lazy var curtainButton: UIButton = {
        let curtainButton = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 50))
        curtainButton.titleLabel?.textColor = .white
        curtainButton.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        curtainButton.addTarget(self, action: #selector(curtainAction), for: .touchUpInside)
        return curtainButton
    }()
    
    func setupCurtainTitle() {
        curtainButton.setTitle("All Cards", for: .normal)
    }
    
    //MARK: - Setup search bar
    private func setupExpandableSearchbar() {
        guard let navController = navigationController else { return }
        searchbarView.translatesAutoresizingMaskIntoConstraints = false
        navigationController?.navigationBar.addSubview(searchbarView)

        rightConstraint = searchbarView.widthAnchor.constraint(equalToConstant: 0)
        rightConstraint.isActive = false

        NSLayoutConstraint.activate([
            searchbarView.topAnchor.constraint(equalTo: navController.navigationBar.topAnchor),
            searchbarView.leftAnchor.constraint(equalTo: navController.navigationBar.leftAnchor, constant: 10),
            searchbarView.bottomAnchor.constraint(equalTo: navController.navigationBar.bottomAnchor)
        ])
    }
    
    //MARK: - Add navigation bar buttons in navigation curtain
    private func setupNavigationItems() {
        let searchButton = UIBarButtonItem(image: UIImage(named: "searh"),
                                           style: .done,
                                           target: self,
                                           action: #selector(searchPet))
        searchButton.tintColor = .white

        navigationItem.rightBarButtonItem = blockButton
        navigationItem.leftBarButtonItem = searchButton
        navigationItem.titleView = curtainButton
        setupCurtainTitle()
    }
    
    //MARK: - Setup shadow to navigation controller and navigationCurtain
    private func setupNavigationBarView() {
        navigationController?.navigationBar.barTintColor = UIColor(red: 111/255, green: 75/255, blue: 51/255, alpha: 0)
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.layer.shadowColor = UIColor.black.cgColor
        navigationController?.navigationBar.layer.shadowOpacity = 1
        navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 2, height: 2)
        navigationController?.navigationBar.layer.shadowRadius = 6
        
        navigationCurtain.layer.masksToBounds = false
        navigationCurtain.layer.shadowColor = UIColor.black.cgColor
        navigationCurtain.layer.shadowOpacity = 1
        navigationCurtain.layer.shadowOffset = CGSize(width: 2, height: 2)
        navigationCurtain.layer.shadowRadius = 6
        self.view.addSubview(navigationCurtain)
    }
    
    //MARK: - Setup navigationCurtain constraints
    private func addConstraintToView() {
        heightNavCurtainExpand = navigationCurtain.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.17)
        heightNavCurtainCollapse = navigationCurtain.heightAnchor.constraint(equalToConstant: 0)

        navigationCurtain.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            navigationCurtain.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            navigationCurtain.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            navigationCurtain.topAnchor.constraint(equalTo: view.topAnchor)
        ])
    }
    
    //MARK: - Navigation bar items actions
    
    //MARK: - Search pets animation
    @objc func searchPet() {
        setupSearchTableView()
        guard let navController = navigationController,
            let cancelWidth = cancelSearchButton.customView?.frame.width
            else { return }
        let rightConstraint = searchbarView.rightAnchor
            .constraint(equalTo: navController.navigationBar.rightAnchor,
                        constant: -cancelWidth * 1.5)
        setupSearchAnimation(layout: rightConstraint,
                             barButton: cancelSearchButton,
                             titleViewHidden: true)
    }
    
    //MARK: - Cancel searching
    @objc func cancelSearch() {
        let rightConstraint = searchbarView.widthAnchor.constraint(equalToConstant: 0)
        setupSearchAnimation(layout: rightConstraint, barButton: blockButton, titleViewHidden: false)
        searchTableView.removeFromSuperview()
    }
    
    private func setupSearchAnimation(layout: NSLayoutConstraint, barButton: UIBarButtonItem, titleViewHidden: Bool) {
        self.navigationItem.titleView?.isHidden = titleViewHidden
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.rightConstraint.isActive = false
            self.rightConstraint = layout
            self.rightConstraint.isActive = true
            self.navigationController?.navigationBar.layoutIfNeeded()
            self.navigationItem.rightBarButtonItem = barButton
        }
    }
        
    //MARK: - Navigation curtain drop down animation
    @objc func curtainAction() {
        self.currentCurtainMode.toogle()
        switch currentCurtainMode {
        case .lift:
            selectCurtain?()
            setupAnimation(hideShadow: true, shadowColor: UIColor.clear.cgColor)
            curtainDidSelect(heightAnchorBefore: false,
                             heightAnchorAfter: true, layout: heightNavCurtainExpand)
            animateLayout(hideShadow: true, shadowColor: UIColor.clear.cgColor)

        case .drop:
            selectCurtain?()
            curtainDidSelect(heightAnchorBefore: false,
                             heightAnchorAfter: true, layout: heightNavCurtainCollapse)
            animateLayout(hideShadow: false, shadowColor: UIColor.black.cgColor)
        }
    }
    
    private func curtainDidSelect(heightAnchorBefore: Bool, heightAnchorAfter: Bool, layout: NSLayoutConstraint) {
           heightCurtainAnchor?.isActive = heightAnchorBefore
           heightCurtainAnchor = layout
           heightCurtainAnchor?.isActive = heightAnchorAfter
       }

    private func setupAnimation(hideShadow: Bool, shadowColor: CGColor) {
        self.navigationController?.navigationBar.setValue(hideShadow, forKey: "hidesShadow")
        self.navigationController?.navigationBar.layer.shadowColor = shadowColor
    }
    
    private func animateLayout(hideShadow: Bool, shadowColor: CGColor) {
        UIView.animate(withDuration: 0.4, animations: { [unowned self] in
            self.view.layoutIfNeeded()
            
        }) { [unowned self] (_) in
            self.setupAnimation(hideShadow: hideShadow, shadowColor: shadowColor)
        }
    }
    
    @objc func blockAction() {
        selectBlock?()
    }
    
    func filterContent(_ searchText: String) {
        filteredPetsList = petsList.filter { pets in
            return pets.lowercased().contains(searchText.lowercased())
        }
        self.searchTableView.reloadData()
    }
}

extension SearchContainerViewController: UISearchBarDelegate, UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContent(searchController.searchBar.text!)
    }
}

extension SearchContainerViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return filteredPetsList.count
        } else {
            return petsList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        if isFiltering {
            cell.textLabel?.text = filteredPetsList[indexPath.row]
        } else {
            cell.textLabel?.text = petsList[indexPath.row]
        }
        return cell
    }
}
