//
//  NavigationStork.swift
//  PetProject
//
//  Created by Anton Hoang on 2/15/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation
import UIKit

enum Category {
    case favourite
    case essentials
    case deepCuts
    case popular
}

class NavigationCurtainView: UIView {

    //MARK: - IBOutlets
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - Properties
    var category: Category? = nil
    var categoryItems = [Category]()
    var presenter: NavigationCurtainPresenter?

    //MARK: - Initial
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        initCollectionView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func commonInit() {
        let bundle = Bundle(for: type(of: self))
        bundle.loadNibNamed("NavigationCurtainView", owner: self, options: nil)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.backgroundColor = UIColor(red: 111/255, green: 75/255, blue: 51/255, alpha: 100)
        contentView.layer.shadowColor = UIColor.gray.cgColor
        contentView.layer.shadowOpacity = 0.3
        contentView.layer.shadowOffset = CGSize(width: 2, height: 2)
        contentView.layer.shadowRadius = 6
        addSubview(contentView)
    }
    
    func initCollectionView() {
        let nib = UINib(nibName: "NavigationItemCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "NavigationItemCell")
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
    }
}
extension NavigationCurtainView: NavigationCurtainInputPresenter {}

extension NavigationCurtainView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    //MARK: - setup colletionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.width
        var cellSizeWidth = width / 4
        cellSizeWidth *= 0.8
        let cellSizeHeight = cellSizeWidth
        return CGSize(width: cellSizeWidth, height: cellSizeHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NavigationItemCell", for: indexPath) as! NavigationItemCell
        if let items = presenter?.categoryList()[indexPath.row] {
            cell.configurate(item: items)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 80
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
    }
    //MARK: - collection view actions
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        categoryItems = [.favourite, .essentials,.deepCuts, .popular]
        let items = categoryItems[indexPath.row]
        
        switch items {
        case .favourite:
            presenter?.showCardsScene()
            print("favourite")
        case .essentials:
            presenter?.showFavoritesScene()
            print("essentials")
        case .deepCuts:
            print("deepCuts")
        case .popular:
            presenter?.showPopularScene()
            print("popular")
        }
    }
}
