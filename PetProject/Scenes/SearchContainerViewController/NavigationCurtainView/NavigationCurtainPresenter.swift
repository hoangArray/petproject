//
//  NavigationCurtainPresenter.swift
//  PetProject
//
//  Created by Anton Hoang on 2/19/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation

protocol NavigationCurtainInputPresenter {}

protocol NavigationCurtainOutputPresenter {
    init(view: NavigationCurtainInputPresenter, router: Routable)
    func showFavoritesScene()
    func categoryList() -> [CategoryItem]
}

class NavigationCurtainPresenter: NavigationCurtainOutputPresenter {
    
    var view: NavigationCurtainInputPresenter
    var router: Routable
    
    required init(view: NavigationCurtainInputPresenter, router: Routable) {
        self.view = view
        self.router = router
    }
    
    func categoryList() -> [CategoryItem] {
        let categories = [CategoryItem(image: "Favorites_iphone", title: "Favorites"),
                          CategoryItem(image: "Essentials_iphone", title: "Essential"),
                          CategoryItem(image: "DeepCuts_iphone-1", title: "Deep Cuts"),
                          CategoryItem(image: "Popular_iphone", title: "Popular")]
        return categories
    }
    
    func showFavoritesScene() {
        router.showFavouritesScene()
     }
    
    func showCardsScene() {
        router.popToRoot()
    }
    
    func showDeepCutsScene() {}
    
    func showEssentialScene() {}
    
    func showPopularScene() {
        router.showPopularScene()
    }
}
