//
//  Searchbar.swift
//  PetProject
//
//  Created by Anton Hoang on 2/22/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit

class SearchbarView: UIView {
    
    //MARK: - Properties
    var searchController: UISearchController!
    
    //MARK: - Lifecicle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSearchBar()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupSearchBar()
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Setup views
    private func setupSearchBar() {
        searchController = UISearchController(searchResultsController: nil)
        addSubview(searchController.searchBar)
        
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.showsCancelButton = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchController.searchBar.tintColor = .white
        searchController.searchBar.barTintColor = .white
        searchController.searchBar.searchTextField.backgroundColor = .white
        
        NSLayoutConstraint.activate([
            searchController.searchBar.topAnchor.constraint(equalTo: topAnchor),
            searchController.searchBar.bottomAnchor.constraint(equalTo: bottomAnchor),
            searchController.searchBar.rightAnchor.constraint(equalTo: rightAnchor),
            searchController.searchBar.leftAnchor.constraint(equalTo: leftAnchor)
        ])
    }
}
