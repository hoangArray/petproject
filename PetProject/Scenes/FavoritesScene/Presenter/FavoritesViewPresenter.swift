//
//  FavoritesViewPresenter.swift
//  PetProject
//
//  Created by Anton Hoang on 2/19/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation

protocol FavoritesViewInputProtocol {}

protocol FavoritesViewOutputProtocol {
    init(view: FavoritesViewInputProtocol, router: Routable)
    var petsList: [Pet] { get set }
}

class FavoritesViewPresenter: FavoritesViewOutputProtocol {
    
    var view: FavoritesViewInputProtocol
    var router: Routable
    var petsList = [Pet]()

    
    required init(view: FavoritesViewInputProtocol, router: Routable) {
        self.view = view
        self.router = router
    }
}
