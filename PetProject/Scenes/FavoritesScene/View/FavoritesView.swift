//
//  FavoritesView.swift
//  PetProject
//
//  Created by Anton Hoang on 2/24/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation
import UIKit

class FavoritesView: UIView {
    
    let tableView = UITableView()
    var presenter: FavoritesViewPresenter?

    //MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Setup UI
    func commonInit() {
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear
        tableView.dataSource = self
        tableView.delegate = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(UINib(nibName: "FavoritesViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.contentInset.bottom = 20
        addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
//    let petsList = [Pets(petImage: "Image", petNameLabel: "Airedale Terrier", petDetailsLabel: "The Airedale is the largest of the British terriers."),
//                    Pets(petImage: "Image-1", petNameLabel: "Aidi", petDetailsLabel: "Dogs of the Affenpinscher type have been known …"),
//                    Pets(petImage: "Image-2", petNameLabel: "Afghan Hound", petDetailsLabel: "The Airedale Terrier is a of the now."),
//                    Pets(petImage: "Image-3", petNameLabel: "Aidi", petDetailsLabel: "The Airedale Terrier is a descendant of the now…"),
//                    Pets(petImage: "Image-4", petNameLabel: "Akbash", petDetailsLabel: "Akbash is accumalate him know how to accurate godzilla"),
//                    Pets(petImage: "Image-5", petNameLabel: "Affenpinscher", petDetailsLabel: "Dogs of the Affenpinscher type have been known …")]
    
    
    var petsList = [Pet]()
}

extension FavoritesView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return petsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FavoritesViewCell
        let items = petsList[indexPath.section]
        cell.configurate(pets: items)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
}

