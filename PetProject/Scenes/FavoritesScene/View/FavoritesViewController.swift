//
//  FavoritesViewController.swift
//  PetProject
//
//  Created by Anton Hoang on 2/14/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit

class FavoritesViewController: SearchContainerViewController {
    //MARK: - Properties
    var favoriteView = FavoritesView()
    var presenter: FavoritesViewPresenter?
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        addConstraintToTableView()
        setupCurtainTitle()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - Setup UI
    private func setupUI() {
        view.backgroundColor = UIColor(red: 247/255, green: 238/255, blue: 225/255, alpha: 12)
    }
    
    override func setupCurtainTitle() {
        curtainButton.setTitle("Favorites", for: .normal)
    }
    
    private func addConstraintToTableView() {
        favoriteView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(favoriteView)
        NSLayoutConstraint.activate([
            favoriteView.topAnchor.constraint(equalTo: navigationCurtain.bottomAnchor),
            favoriteView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            favoriteView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            favoriteView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

extension FavoritesViewController: FavoritesViewInputProtocol {}
