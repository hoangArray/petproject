//
//  FavoritesViewCell.swift
//  PetProject
//
//  Created by Anton Hoang on 2/14/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit

class FavoritesViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var petImage: UIImageView!
    @IBOutlet weak var petName: UILabel!
    @IBOutlet weak var petDetail: UILabel!
    @IBOutlet weak var petView: UIView!
    
    //MARK: - Lifecicle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        petView.layer.cornerRadius = 10
        petView.layer.shadowColor = UIColor.black.cgColor
        petView.layer.shadowOpacity = 0.5
        petView.layer.shadowOffset = CGSize(width: 0, height: 1)
        petView.layer.shadowRadius = 1
        backgroundColor = UIColor(red: 247/255, green: 238/255, blue: 225/255, alpha: 12)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - set a cells
    func configurate(pets: Pet) {
        petImage.contentMode = .scaleAspectFill
        petName.text = pets.petNameLabel
        petImage.image = UIImage(named: pets.petImage)
        petDetail.text = pets.petDetailsLabel
    }
    
}
