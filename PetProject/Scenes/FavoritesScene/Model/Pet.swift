//
//  Model.swift
//  PetProject
//
//  Created by Anton Hoang on 2/20/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation

struct Pet {
    var petImage: String
    var petNameLabel: String
    var petDetailsLabel: String
}
