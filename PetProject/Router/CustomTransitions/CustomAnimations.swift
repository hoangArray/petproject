//
//  CustomAnimations.swift
//  PetProject
//
//  Created by Anton Hoang on 2/18/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit

class FadePushAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    private lazy var animator: UIViewPropertyAnimator = {
           return UIViewPropertyAnimator(duration: 0.5, curve: .easeInOut)
       }()
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
//        guard let fromViewController = transitionContext.viewController(forKey: .from) as? CardsViewController else { return }
        
        guard let toViewController = transitionContext.viewController(forKey: .to) as? DetailViewController else { return }
        
        transitionContext.containerView.addSubview(toViewController.view)
        toViewController.view.alpha = 0
        
//        let selectedCell = fromViewController.selectedCell
//        let petImageDetail = toViewController.petCardImage_1

        let duration = self.transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration,
                       animations: {
                        toViewController.view.alpha = 1

        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}

class FadePopAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromViewController = transitionContext.viewController(forKey: .from),
            let toViewController = transitionContext.viewController(forKey: .to)
            else { return }
        
        transitionContext.containerView.insertSubview(toViewController.view,
                                                      belowSubview: fromViewController.view)
        
        let duration = self.transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration,
                       animations: {
                        fromViewController.view.alpha = 0
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}
