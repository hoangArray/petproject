//
//  ZoomViewControllerDelegate.swift
//  PetProject
//
//  Created by Anton Hoang on 2/28/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation
import UIKit

extension Router: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        switch operation {
        case .push:
            if toVC is DetailViewController {
                return FadePushAnimator()
            }
            return nil
        case .pop:
            if fromVC is DetailViewController {
                return FadePopAnimator()
            }
            return nil
        default:
            return nil
        }
        
    }
}
