//
//  RouterProtocol.swift
//  PetProject
//
//  Created by Anton Hoang on 2/13/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation
import UIKit

protocol RouterMain {
    var navigationController: UINavigationController? { get set }
    var assemblyBuilder: AssemblyBuilderProtocol? { get set }
}

protocol Routable: RouterMain {
    func instantiateCardScene()
    func showFavouritesScene()
    func showDetailScene(_ withCard: Pet)
    func popToCardScene()
    func showPopularScene()
    func popToRoot()
    
    var petList: [Pet]? { get set }
}

class Router: NSObject, Routable {
    
    var navigationController: UINavigationController?
    var assemblyBuilder: AssemblyBuilderProtocol?
//    private var transition: UIViewControllerAnimatedTransitioning?
    var petList: [Pet]?
    
    init(navigationController: UINavigationController, assemblyBuilder: AssemblyBuilderProtocol) {
        super.init()
        self.navigationController = navigationController
        self.assemblyBuilder = assemblyBuilder
        self.navigationController?.delegate = self
    }
    
    func instantiateCardScene() {
        guard let navContoller = navigationController,
            let cardsVC = assemblyBuilder?.createCardsScene(router: self) else { return }
        navContoller.viewControllers = [cardsVC]
    }
    
    func showFavouritesScene() {
        guard let navController = navigationController,
            let favVC = assemblyBuilder?.createFavouritesScene(router: self, petList: petList ?? []) else { return }
        if (navController.topViewController?.isKind(of: FavoritesViewController.self))! {
            return
        } else {
            navController.pushViewController(favVC, animated: false)
        }
    }
    
    func showDetailScene(_ withCard: Pet) {
        guard let navContoller = navigationController,
            let detVC = assemblyBuilder?.createDetailScene(router: self, withCard: withCard) else { return }
        navContoller.pushViewController(detVC, animated: true)
    }
    
    func popToCardScene() {
        guard let navController = navigationController else { return }
        let cardsVC = navController.viewControllers.filter { $0 is CardsViewController }.first as! CardsViewController
            navController.popToViewController(cardsVC, animated: false)
            cardsVC.collapse()
    }
    
    func popToRoot() {
        guard let navController = navigationController else { return }
        navController.popToRootViewController(animated: false)
    }
    
    func showPopularScene() {
        guard let navController = navigationController,
            let popularVC = assemblyBuilder?.createPopularScene(router: self) else { return }
        if (navController.topViewController?.isKind(of: PopularViewController.self))! {
            return
        } else {
            navController.pushViewController(popularVC, animated: false)
        }
    }
}

