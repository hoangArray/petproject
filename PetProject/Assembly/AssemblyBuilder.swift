//
//  AssemblyBuilder.swift
//  PetProject
//
//  Created by Anton Hoang on 2/13/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation
import UIKit

protocol AssemblyBuilderProtocol {
    func createCardsScene(router: Routable) -> UIViewController
    func createFavouritesScene(router: Routable, petList: [Pet]) -> UIViewController
    func createDetailScene(router: Routable, withCard: Pet) -> UIViewController
    func createPopularScene(router: Routable) -> UIViewController
}

class AssemblyBuilder: AssemblyBuilderProtocol {

    private func assemblyNavigationCurtain(router: Routable) -> NavigationCurtainView {
        let navigationCurtain = NavigationCurtainView()
        let navCurtainPresenter = NavigationCurtainPresenter(view: navigationCurtain, router: router)
        navigationCurtain.presenter = navCurtainPresenter
        return navigationCurtain
    }
    
    func createCardsScene(router: Routable) -> UIViewController {
        let cardsVC = CardsViewController()
        let cardsPresenter = CardsViewPresenter(view: cardsVC, router: router)
        cardsVC.navigationCurtain = assemblyNavigationCurtain(router: router)
        cardsVC.presenter = cardsPresenter
        return cardsVC
    }
    
    func createFavouritesScene(router: Routable, petList: [Pet]) -> UIViewController {
        let favVC = FavoritesViewController()
        let favPresenter = FavoritesViewPresenter(view: favVC, router: router)
        favVC.navigationCurtain = assemblyNavigationCurtain(router: router)
        favVC.presenter = favPresenter
        favVC.favoriteView.petsList = petList
        return favVC
    }
    
    func createDetailScene(router: Routable, withCard: Pet) -> UIViewController {
        let detVC = DetailViewController()
        let presenter = DetailViewPresenter(view: detVC, router: router, withCard: withCard)
        detVC.presenter = presenter
        return detVC
    }
    
    func createPopularScene(router: Routable) -> UIViewController {
        let popularVC = PopularViewController()
        let popularPresenter = PopularViewPresenter(view: popularVC, router: router)
        popularVC.navigationCurtain = assemblyNavigationCurtain(router: router)
        popularVC.presenter = popularPresenter
        return popularVC
    }
}
