//
//  SceneDelegate.swift
//  PetProject
//
//  Created by Anton Hoang on 2/13/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        window = UIWindow(windowScene: windowScene)
        
        let navigationController = UINavigationController()
        let assemblyBuilder = AssemblyBuilder()
        let router = Router(navigationController: navigationController, assemblyBuilder: assemblyBuilder)
        router.instantiateCardScene()
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}

